from django.db import models
from datetime import datetime, date

class Status(models.Model):
    
    status = models.CharField(max_length=300)
    date = models.DateTimeField(default=datetime.now)

    def __str__(self):
        return self.status

