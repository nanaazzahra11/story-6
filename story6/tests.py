from django.test import TestCase
from django.test import TestCase
from django.urls import resolve
from django.test.client import Client
from .views import status_create
from .views import status_delete
from .models import Status
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

class UnitTestStory6(TestCase):
    def test_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, status_create)

    def test_using_to_do_list_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'story6.html')

    def test_post_status_using_to_do_list_template(self):
        response = Client().post('',{'status':'TEST'})
        self.assertTemplateUsed(response, 'story6.html')

    def test_model_can_create_new_status(self):
        new_status = Status.objects.create(status='Aku capek')

        counting_all_available_status = Status.objects.all().count()
        self.assertEqual(counting_all_available_status, 1)

    def test_str_is_equal_to_status(self):
        statusNew = Status.objects.create(status='Test')
        status = Status.objects.get(status='Test')
        self.assertEqual(str(status), status.status)

    def test_delete_status(self):
        obj = Status(status="asdasdasd")
        obj.save()

        response = self.client.get("/delete/")

        self.assertEqual(Status.objects.all().count(),0)

class FunctionalTestStory6(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options = chrome_options)
        self.selenium.get('http://localhost:8000')

    def tearDown(self):
        self.selenium.quit()
    
    # def test_open_page(self):
    #     browser = self.selenium
    #     # self.assertIn("Welcome!",browser.title)
    
    def test_status(self):
        browser = self.selenium
        str_input = "functional teeeeeeest"
        text_area = browser.find_element_by_name("status")
        time.sleep(2)
        text_area.send_keys(str_input)
        time.sleep(2)
        sub_button = browser.find_element_by_id("add")
        sub_button.click()
        time.sleep(2)
        self.assertIn(str_input,browser.page_source)




