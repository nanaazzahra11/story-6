from django.shortcuts import render, redirect
from .models import Status
from .import forms

# def story6(request):
#     return render(request, 'story6.html')

# def status(request):
#     # status = Status.objects.all()
#     # status = Status.objects.all().order_by('date')
#     return render(request, 'story6.html', {'status': status})

def status_create(request):
    status = Status.objects.all().order_by('date')
    if request.method == 'POST':
        form = forms.StatusForm(request.POST)
        if form.is_valid():
            form.save()
            return render(request, 'story6.html', {'form': form, 'status': status})

    else:
        form = forms.StatusForm()
    return render(request, 'story6.html', {'form': form, 'status':status})

def status_delete(request):
	Status.objects.all().delete()
	return redirect('story6:status_create')