from django.urls import path
from . import views

app_name = 'story6'

urlpatterns = [
    path('', views.status_create, name='status_create'),
    path('delete/', views.status_delete, name='status_delete'),
]